﻿
using System;
using Word2vec;
using Word2vec.Tools;
using Microsoft.VisualBasic.FileIO;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net;

namespace wordtovec
{
    struct Essay
    {
        public string text;
        public int[] personality;
        public float[] vector;

    }
    struct StructToLearn
    {
        public double[] In;
        public double[] Out;
    }
    class Program
    {
        static Essay[] parseFile(string path)
        {
            List<Essay> essays = new List<Essay>();
            using (TextFieldParser parser = new TextFieldParser(path))
            {
                parser.Delimiters = new string[] { "," };
                while (true)
                {
                    string[] parts = parser.ReadFields();
                    if (parts == null)
                    {
                        break;
                    }
                    Essay ess = new Essay();
                    int[] pers = new int[5];
                    float[] vec = new float[300];
                    ess.text = parts[1];
                    for (int i = 2; i < 7; i++)
                    {
                        pers[i - 2] = (parts[i] == "y" ? 1 : 0);
                    }
                    ess.vector = sum(ess.text);
                    ess.personality = pers;
                    essays.Add(ess);
                  //  Console.WriteLine("Essay:{5} \n personality: {0},{1},{2},{3},{4} field(s)\n", ess.personality[0], ess.personality[1], ess.personality[2], ess.personality[3], ess.personality[4], ess.text);
                }
            }
            return essays.ToArray();
        }
        static void printArray(float[] arr)
        {
            Console.WriteLine();
            Console.Write("[");
            foreach(float a in arr)
            {
                Console.Write(a.ToString() + ", ");
            }
            Console.WriteLine("]\n");
        }
        static double distance(string a, string b)
        {
            if (!(vocabulary.ContainsWord(a) && vocabulary.ContainsWord(b)))
                return -10;
            var word = vocabulary[a];
            var word2 = vocabulary[b];
           // Console.WriteLine("Cosine distance "+ word.GetCosineDistanceTo(word2).DistanceValue);
           // Console.WriteLine("Cosine distance (by substraction) " + word.Substract(word2).MetricLength);
           // Console.WriteLine("Cosine distance (by substraction) vice-versa " + word2.Substract(word).MetricLength);
            return word.GetCosineDistanceTo(word2).DistanceValue;
        }
        static void vector(string a)
        {
            var word = vocabulary[a];
            printArray(word.NumericVector);
        }
        static void closest(string a)
        {
            var words = vocabulary.Distance(a, 5);
            foreach(var neightboor in words)
            {
                Console.WriteLine("Nearest " + neightboor.Representation.WordOrNull + "\t\t" + neightboor.DistanceValue);
            }
        }
        static float[]  sum(string a)
        {
            var arr = a.Split(' ', '.',',');
            float[] vector = new float[300];
            foreach (var word in arr)
            {
                if (!vocabulary.ContainsWord(word))
                    continue;
                var currWord = vocabulary[word];
                vector = sumVectors(vector, currWord.NumericVector);
            }
            //Console.WriteLine("Summary representation");
           // printArray(vector);
            return vector;
            /*/
            var closest = vocabulary.Distance(new Representation(vector),5);
            foreach (var neightboor in closest)
            {
                Console.WriteLine("Nearest " + neightboor.Representation.WordOrNull + "\t\t" + neightboor.DistanceValue);
            }/*/

        }
        static void sum2(string a)
        {
            var arr = a.Split(' ', '.', ',','?','!','/');
            Representation repr = null;
            foreach (var word in arr)
            {                
                if (!vocabulary.ContainsWord(word))
                    continue;
                var currWord = vocabulary[word];

                if (repr == null)
                    repr = currWord;
                else
                    repr = repr.Add(currWord);
            }
            Console.WriteLine("Summary representation");
            printArray(repr.NumericVector);
            var closest = vocabulary.Distance(repr, 5);
            foreach (var neightboor in closest)
            {
                Console.WriteLine("Nearest " + neightboor.Representation.WordOrNull + "\t\t" + neightboor.DistanceValue);
            }

        }
        static float[] sumVectors(float[] a, float[] b)
        {
            float[] resultVector = new float[300];
            for(int i = 0; i < 300; i++)
            {
                resultVector[i] = a[i]/2 + b[i]/2;
            }
            return resultVector;
        }
        static void read()
        {
            Console.WriteLine("Reading the model... wait");
            //Set your w2v bin file path here:

            var path = @"C:/GoogleNews-vectors-negative300.bin";
            //var path = "vectors.bin";

            vocabulary = new Word2VecBinaryReader().Read(path);
            Essay[] essays = parseFile("essays.csv");

            //For w2v text sampling file use:
            //var vocabulary = new Word2VecTextReader().Read(path);

            Console.WriteLine("vectors file: " + path);
            Console.WriteLine("vocabulary size: " + vocabulary.Words.Length);
            Console.WriteLine("w2v vector dimensions count: " + vocabulary.VectorDimensionsCount);
            TextWriter sw = new StreamWriter("Data3.csv");

            foreach (Essay essay in essays)
            {
                StringBuilder line = new StringBuilder("");
                foreach (float ind in essay.vector)
                {
                    line.Append(ind + ", ");
                }
                foreach (int ind in essay.personality)
                {
                    line.Append(ind + ", ");
                }
                line.Remove(line.Length - 2, 2);
                line = line.Remove(line.Length - 2, 2);
                sw.WriteLine(line);
            }
            
            sw.Flush();
            Console.WriteLine("Done!");
        }

        static Vocabulary vocabulary;

        static void Main(string[] args)
        {
            var path = @"GoogleNews-vectors-negative300.bin";
            // var path = "vectors.bin";
            vocabulary = new Word2VecBinaryReader().Read(path);

            /*/
            List<double[]> inp = new List<double[]>();
            List<double[]> outp = new List<double[]>();

            List<StructToLearn> stlList = new List<StructToLearn>();
            using (TextFieldParser parser = new TextFieldParser("Data2.csv"))
            {
                parser.Delimiters = new string[] { "," };
                while (true)
                {
                    string[] parts = parser.ReadFields();
                    if (parts == null)
                    {
                        break;
                    }
                    StructToLearn stl = new StructToLearn();
                    double[] Out = new double[5];
                    double[] In = new double[300];
                    for (int i = 0; i < 300; i++)
                    {
                       double.TryParse(parts[i], out In[i]);
                    }
                    for (int i = 300; i < 305; i++)
                    {
                        double.TryParse(parts[i], out Out[i-300]);
                    }
                    stl.In = In;
                    stl.Out = Out;
                    stlList.Add(stl);
                    inp.Add(In);
                    outp.Add(Out);
                }
            }
            // Since we would like to learn binary outputs in the form
            // [-1,+1], we can use a bipolar sigmoid activation function
            IActivationFunction function = new BipolarSigmoidFunction();

            // In our problem, we have 2 inputs (x, y pairs), and we will 
            // be creating a network with 5 hidden neurons and 1 output:
            //;
            var inputs = inp.ToArray();
            var outputs = outp.ToArray();

            var network = new ActivationNetwork(function,
                inputsCount: 300, neuronsCount: new[] { 7, 5 });

            // Create a Levenberg-Marquardt algorithm
            var teacher = new LevenbergMarquardtLearning(network)
            {
                UseRegularization = true
            };

            // Because the network is expecting multiple outputs,
            // we have to convert our single variable into arrays
            //
            var y = outputs;

            // Iterate until stop criteria is met
            double error = double.PositiveInfinity;
            double previous;
            Console.WriteLine("Start");
            int rr = 1;
            do
            {
               rr++;
                previous = error;

                // Compute one learning iteration
                error = teacher.RunEpoch(inputs, y);
                Console.WriteLine(error);

                // } while ();
            } while (Math.Abs(previous - error) >= 1e-9 * previous);
            Console.WriteLine("Fin");

            for(int i = 1; i < 10; i++) { 
}
            // Classify the samples using the model
            double[][] answers = inputs.Apply(network.Compute);
            /*/
            /*/  foreach(double[] anstop in answers)
              {
                  foreach(double ansbot in anstop)
                  {
                      Console.Write(ansbot + " ");
                  }
                  Console.WriteLine();
              }/*/

            // Plot the results
            // ScatterplotBox.Show("Expected results", inputs, outputs);
            //  ScatterplotBox.Show("Network results", inputs, answers)
            // .Hold();

            /*/ var numInputs = 300;
             var numOutputs = 5;
             var numHiddenLayers = 0;
             var numNeuronsInHiddenLayer = 0;
             INeuralNetwork network = NeuralNetworkFactory.GetInstance().Create(numInputs, numOutputs, numHiddenLayers, numNeuronsInHiddenLayer);
             network.SetInputs();/*/



            //read();

           
            Console.WriteLine();
            do {
                HttpListener listener = new HttpListener();
                listener.Prefixes.Add("http://localhost:8080/");
                listener.Start();
                // метод GetContext блокирует текущий поток, ожидая получение запроса 
                HttpListenerContext context = listener.GetContext();
                HttpListenerRequest request = context.Request;
                var str = request.QueryString;
                Console.WriteLine(DateTime.Now.ToLocalTime() +": "+ request.Url + " " + request.UrlReferrer);

                if (str["word1"] == null)
                {
                    Console.WriteLine("(Bad may be)");
                    listener.Stop();
                    continue;
                }
                Console.WriteLine(str["word1"] +" "+ str["word2"]+" distance:"+distance(str["word1"],str["word2"]));
                HttpListenerResponse response = context.Response;
                double responseStr = distance(str["word1"], str["word2"]);
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(""+responseStr);
                response.ContentLength64 = buffer.Length;
                Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                output.Close();
                listener.Stop();
                /*/
                //Console.Read();
                string req = Console.ReadLine();
                string[] reqArr = req.Split(':');

                if (reqArr[0] == "vector")
                {
                    vector(reqArr[1]);
                } else
                if (reqArr[0] == "distance")
                {
                    distance(reqArr[1],reqArr[2]);
                } else
                if (reqArr[0] == "closest")
                {
                    closest(reqArr[1]);
                } else
                if (reqArr[0]=="sum")
                {
                    if(!(reqArr[1]=="from file"))
                        sum(reqArr[1]);
                    else
                    {
                        path = "example.txt";
                        var text = System.IO.File.ReadAllText(path);
                        sum(text);
                    }
                } else
                if (reqArr[0] == "sum2")
                {
                    if (!(reqArr[1] == "from file"))
                        sum2(reqArr[1]);
                    else
                    {
                        path = "example.txt";
                        var text = System.IO.File.ReadAllText(path);
                        sum2(text);
                    }
                }
                else
                {
                    Console.WriteLine("wrong request");
                }
            /*/
            } while (true);
            
            Console.WriteLine("Representation vector of man ");
            printArray(vocabulary["man"].NumericVector);
            Console.WriteLine("Metric Length "+vocabulary["man"].MetricLength);

            var maleman= vocabulary.Distance(vocabulary["male"].Substract(vocabulary["man"]),1);
            var manmale = vocabulary.Distance(vocabulary["man"].Substract(vocabulary["male"]), 1);

            foreach (var neightboor in maleman)
                Console.WriteLine("male-man = "+neightboor.Representation.WordOrNull + "\t\t" + neightboor.DistanceValue);
            Console.WriteLine("male-man" + vocabulary["male"].Substract(vocabulary["man"]).MetricLength);

            foreach (var neightboor in manmale)
                Console.WriteLine("man-male = "+neightboor.Representation.WordOrNull + "\t\t" + neightboor.DistanceValue);
            Console.WriteLine("man-male" + vocabulary["man"].Substract(vocabulary["male"]).MetricLength);


            


            // Is simmilar to:
            // var closest = vocabulary[boy].GetClosestFrom(vocabulary.Words.Where(w => w != vocabulary[boy]), count);

          
            Console.WriteLine();

            
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            Console.ReadKey();
            Console.ReadKey();
            Console.ReadKey();


        }
    }
}
